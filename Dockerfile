#FROM oracle/graalvm-ce:20.1.0-java11 as graalBuild
#RUN gu install native-image
#COPY src /usr/src/app/src
#COPY .mvn /usr/src/app/.mvn
#COPY mvnw /usr/src/app
#COPY pom.xml /usr/src/app
#WORKDIR /usr/src/app
#RUN GRAALVM_HOME=/graalvm ./mvnw package -DskipTests=true -Pnative -Dnative-image.docker-build=true
#
#FROM alpine:3.9
#EXPOSE 8080
#COPY --from=graalBuild "/usr/src/app/target/code-with-quarkus-*-runner" /code-with-quarkus
#RUN apk add libc6-compat libstdc++
#CMD ./code-with-quarkus


FROM quay.io/quarkus/centos-quarkus-maven:20.1.0-java11 AS build
COPY pom.xml /usr/src/app/
RUN mvn -f /usr/src/app/pom.xml -B de.qaware.maven:go-offline-maven-plugin:1.2.5:resolve-dependencies
COPY src /usr/src/app/src
USER root
RUN chown -R quarkus /usr/src/app
USER quarkus
RUN mvn -f /usr/src/app/pom.xml -Pnative clean package

FROM registry.access.redhat.com/ubi8/ubi-minimal
WORKDIR /work/
COPY --from=build /usr/src/app/target/*-runner /work/application

RUN chmod 775 /work /work/application \
  && chown -R 1001 /work \
  && chmod -R "g+rwX" /work \
  && chown -R 1001:root /work

EXPOSE 8080
USER 1001

CMD ["./application", "-Dquarkus.http.host=0.0.0.0"]