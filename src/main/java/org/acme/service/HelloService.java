package org.acme.service;

import org.acme.payload.Hello;
import org.acme.repository.HelloRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class HelloService {

    @Inject
    HelloRepository helloRepository;

    public List<Hello> list(){
        return helloRepository.findAll().list();
    }

    public long count() {
        return helloRepository.count();
    }
}
