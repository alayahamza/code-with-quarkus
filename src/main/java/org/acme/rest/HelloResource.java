package org.acme.rest;

import org.acme.payload.Hello;
import org.acme.service.HelloService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/hellos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HelloResource {

    @Inject
    HelloService helloService;

    @GET
    public List<Hello> hello() {
        return helloService.list();
    }

    @GET
    @Path("/count")
    public long count() {
        return helloService.count();
    }
}