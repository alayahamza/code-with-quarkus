package org.acme.repository;

import io.quarkus.mongodb.panache.PanacheMongoRepository;
import org.acme.payload.Hello;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class HelloRepository implements PanacheMongoRepository<Hello> {

}